# role chrome

Installs Google Chrome

### Debian / Ubuntu

* chrome_package:  Name of the Google Chrome package
* chrome_repo: The DEB repository for Google Chrome.
* chrome_repo_key:  The DEB repository key for Google Chrome.
* chrome_repo_id: The DEB repository ID for Google Chrome.

### RedHat / CentOS / Amazon

* chrome_package:  Name of the Google Chrome package
* chrome_rep_baseurl: The YUM repository for Google Chrome.
* chrome_repo_gpgkey:  The YUM repository key for Google Chrome.

### MacOSX

* chrome_package:  Name of the Google Chrome package (homebrew cask)
