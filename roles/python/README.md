# role python

Installs Python development tools.

This generally installs the `asdf` utility with the python plugin, which can then be 
used to install a variety of Python and Python-related environments.

### Debian / Ubuntu / RedHat / CentOS

* python_packages: List of system packages to install.
* python_versions: List of Python versions to install using asdf
* python_global_version: Python version to activate in asdf

### Amazon

* python_extras: List of Amazon Linux Extras to enable
* python_packages: List of system packages to install.
* python_versions: List of Python versions to install using asdf.
* python_global_version: Python version to activate in asdf

### MacOSX

* python_packages: List of homebrew packages to install.
* python_versions: List of Python versions to install using asdf.
* python_global_version: Python version to activate in asdf
